<?php

namespace app\models;

/**
 * This is the model class for table "authors".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $middle_name
 *
 * @property JournalsAuthors[] $journalsAuthors
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authors';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \app\components\behaviors\ManyToManyBehavior::class,
                'relations' => [
                    'journals' => 'journal_list',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name', 'middle_name'], 'string', 'max' => 255],
            [['first_name'], 'string', 'min' => 3],
            [['journal_list'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Name',
        ];
    }

    /**
     * Gets query for [[JournalsAuthors]].
     *
     *
     */
    public function getJournals()
    {
        return $this->hasMany(Journals::class, ['id' => 'journal_id'])
            ->viaTable('journals_authors', ['author_id' => 'id']);
    }

    public function fullName(){
        return trim("{$this->first_name} {$this->last_name} {$this->middle_name}");
    }

    /**
     * {@inheritdoc}
     * @return AuthorsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AuthorsQuery(get_called_class());
    }
}
