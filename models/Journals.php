<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "journals".
 *
 * @property int $id ID
 * @property string $title title
 * @property string|null $short_description short description
 * @property string|null $image image
 * @property string $issue_date Journal Issue Date
 * @property string $author_list authors list
 */
class Journals extends \yii\db\ActiveRecord
{
    public $imageFile;
    public $uploadDir = '@webroot/upload/';
    public $imageUrl = '@web/upload/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'journals';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \app\components\behaviors\ManyToManyBehavior::class,
                'relations' => [
                    'authors' => 'author_list',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'issue_date', 'author_list'], 'required'],
            [['issue_date'], 'date', 'format' => 'php:Y.m.d'],
            [['title', 'short_description', 'image'], 'string', 'max' => 255],
            [['author_list'], 'each', 'rule' => ['integer']],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxSize' => 1024*1024*2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'image' => 'Image',
            'issue_date' => 'Issue Date',
            'author_list'=>'Authors'
        ];
    }

    /**
     * Gets query for [[JournalsAuthors]].
     *
     * @return \yii\db\ActiveQuery|JournalsAuthorsQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Authors::class, ['id' => 'author_id'])
            ->viaTable('journals_authors', ['journal_id' => 'id']);
    }

    /**
     * after save
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->upload();
        return parent::beforeSave($insert);
    }

    /**
     * after delete
     */
    public function afterDelete()
    {
        if (is_file(Yii::getAlias($this->uploadDir, true) . '/' . $this->image)) {
            unlink(Yii::getAlias($this->uploadDir, true) . '/' . $this->image);
        }
        parent::afterDelete();
    }

    /**
     * Upload image
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            if (!is_dir(Yii::getAlias($this->uploadDir))) {
                mkdir(Yii::getAlias($this->uploadDir), 0755, true);
            }
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            if ($this->imageFile) {
                $name = uniqid() . '.' . $this->imageFile->extension;
                $this->imageFile->saveAs(Yii::getAlias($this->uploadDir, true) . $name);
                if (is_file(Yii::getAlias($this->uploadDir, true) . $this->image)) {
                    unlink(Yii::getAlias($this->uploadDir, true) . $this->image);
                }
                $this->image = $name;
                return true;
            }
        } else {
            return false;
        }
    }

    public function getImagePath(){
        return Yii::getAlias($this->uploadDir, true) . $this->image;
    }

    public function getImageUrl(){
        return Yii::getAlias($this->imageUrl.$this->image);
    }

    /**
     * {@inheritdoc}
     * @return JournalsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new JournalsQuery(get_called_class());
    }
}
