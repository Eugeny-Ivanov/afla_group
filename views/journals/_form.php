<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Journals */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="journals-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>
    <?php if (is_file($model->getImagePath())) {
        echo Html::img($model->getImageUrl(), ['style' => 'width:200px']);
        echo Html::a('Delete', ['delete-image', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'style'=>'margin-left:10px',
            'data' => [
                'method' => 'post',
                'confirm' => 'Are you sure you want to delete this item?',
            ]

        ]);
    } ?>
    <?= $form->field($model, 'imageFile')->fileInput() ?>
    <?= $form->field($model, 'issue_date')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy.MM.dd',
    ]) ?>


    <?= $form->field($model, 'author_list')->widget(\yii2mod\chosen\ChosenSelect::class, [
        'items' => ArrayHelper::map(
            \app\models\Authors::find()->select('id, first_name')->orderBy('first_name')->asArray()->all(),
            'id',
            'first_name'
        ),
        'options' => [
            'width' => '95%',
            'multiple' => true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
