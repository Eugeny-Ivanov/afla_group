<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JournalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Journals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journals-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Journals', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id'=>'grid-journal',]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([

        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'short_description',
            [
                'attribute' => 'image',
                'format' => ['image', ['width' => '100',]],
                'value' => function ($data) {
                    //Yii::getAlias($this->uploadDir
                    if (is_file($data->getImagePath())) {
                        return Yii::getAlias($data->getImageUrl());
                    }
                },
                'contentOptions' => array('style' => 'text-align:center'),

            ],
            [
                'attribute' => 'issue_date', 'format' => ['date', 'php:d-m-Y']],
            [
                'attribute' => 'authors',
                'value' => function ($data) {
                    $names = array_map(function ($el) {
                        return $el->fullName();
                    }, $data->authors);
                    return join(', ', $names);

                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
