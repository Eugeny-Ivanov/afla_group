<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthorsSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Url;

$this->title = 'Authors';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="authors-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Create Author', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'first_name',
                'last_name',
                'middle_name',

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete} {list}',
                    'buttons' => [
                        'list' => function ($url, $model) {
                            $url = Url::to(['journals/index', 'JournalsSearch[author]' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-list"></span>', false, [
                                'title' => "Journals of author - {$model->fullName()}",
                                'style' => 'cursor:pointer',
                                'class' => 'journals',
                                //'data-toggle'=> 'modal',
                                'data-target' => '#id-modal-main',
                                'data' => [
                                    'url-submission' => $url,
                                    'author'=>$model->fullName()
                                ],
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

<?php
$header = '
<h3 id="head-text"></h3>';
yii\bootstrap\Modal::begin([
    'id' => 'id-modal-main',
    'size' => yii\bootstrap\Modal::SIZE_LARGE,
    'header'        => $header,
    'headerOptions' => ['id' => 'modalHeader'],
]);
echo "<div id='modal-content-details'>";
echo "    <div style='text-align:center'>";
echo "        <span class='fa fa-refresh fa-spin fa-1x fa-fw'></span>" . 'Loading Data...';  // Loading icon
echo "    </div>";
echo "</div>";
yii\bootstrap\Modal::end();

$jsBlock = <<<JS
$('.journals').on('click', function() {    
        $('#id-modal-main').modal('show')        
        .find('#modal-content-details')
        .load($(this).attr('data-url-submission'));  
        $('#id-modal-main').find('#head-text').text($(this).attr('data-author'));
});
JS;
$this->registerJS($jsBlock, \yii\web\View::POS_END);
